<?php
/**
 * Template name: Case studies
 */

get_header();
if (have_posts()) {
	while (have_posts()) {
		the_post();
?>

<main class="case_studies">
    <div class="wrapper">
        <h1 class="header-main u-marginBottom-big"><?php the_title(); ?></h1>
        
        <?php
			$args = array('child_of' => 8);
			$categories = get_categories($args);
			foreach($categories as $category) { ?>
				
			<section class="galleryBlock">
	            <h2 class="gallery_heading"><?php echo $category->name ?></h2>
	            <div class="gallery">
	            	<?php
	            		$myposts = get_posts([
	                      'posts_per_page' => -1,
	                      'category' => $category->term_id,
	                      'orderby' => 'date',
	                      'order' => 'ASC'
	                    ]);
						foreach ($myposts as $post) : ?>
						  <div class="gallery__wrapper">
							<a href="<?php the_permalink(); ?>" class="gallery__link">
								<div class="gallery__img" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>
								<p class="gallery__img-text"><?php the_title(); ?></p>
							</a>
						</div>
	          	<?php endforeach; ?>     
	            </div>
        	</section>
		<?php	}     
        ?>

    </div>

</main>

<?php
	} // end while
} // end if
get_footer();