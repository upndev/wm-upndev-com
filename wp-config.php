<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

if (strpos($_SERVER['HTTP_HOST'], 'upndev.com') !== false) {
	define('DB_NAME', 'upndev_wm');
	define('DB_USER', 'upndev_wm');
	define('DB_PASSWORD', 'Aoy9zBoz');	
}
else {
	define('DB_NAME', 'upndev_wm');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'root');
}

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6zD[$@?E^dwbxc97|)K^XOn>}::,+LT@|8 7mv<Yh14Z5H}{)FWwn,h.eIJAIBc}');
define('SECURE_AUTH_KEY',  'hF^(3o@&=-fOn~j)(J()4^nbRMu!L%KTmq=t8Wh3)()Tp}-fW/ b^8FID$_=8F+~');
define('LOGGED_IN_KEY',    'Rv/%-WP~A3cIuH~ZRSFjmp;f1Q6gX39ArGa_^SPn20c=C?9wtn>e[Y&)GqO_we~h');
define('NONCE_KEY',        'ZC&qz1=(;:;,zOX]/ct[vL5-9h6y:Olzl06P%Ei`A`-63fx|P)}IoA~z&&tP3~?{');
define('AUTH_SALT',        'xyIVrb.{Fl@ViJj(GU(G-7<69[s!Rny/p=nqW+MhlbCk,D9h4!((m]Bh;0$@mty)');
define('SECURE_AUTH_SALT', '|ld#5&@=IAvhg(g?*VJ/WPh4&PJ_~a/6-97.`-%$b3JYpT;Ap>=a<,nN8CX*g}_/');
define('LOGGED_IN_SALT',   'qV9+KBu&]Y5o/a@IZ1tW 5t#[.K4eH$4p0$;PR)KmqZHu=6DoT`z.7pG!g(~u:g[');
define('NONCE_SALT',       'Z1tA^euS1*lFvJs3f=8N$f(R,Cw47S1vRXW}/&r/Yt}3$%C~<JcT{MV!yRIEP*rX');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
