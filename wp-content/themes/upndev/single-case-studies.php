<?php
get_header();
if (have_posts()) {
	while (have_posts()) {
		the_post();
?>
<main class="case_study">
    <div class="wrapper">
        <h1 class="header-main u-marginLeft-h1">
            <?php the_title(); ?>
        </h1>

        <div class="container">
            <div class="container-left">
                <h2 class="header-block"><?php the_field('team_header_text'); ?></h2>
                <ul class="left__list">
                	<?php
                	$currentCategory = get_the_category();
					$subID = $currentCategory[0]->term_id;
					if ($subID == 8) $subID = $currentCategory[1]->term_id;
					if ($subID == 9) $catID = 5; elseif ($subID == 10) $catID = 6;
                      $myposts = get_posts([
                        'posts_per_page' => -1,
                        'category' => $catID,
                        'orderby' => 'date',
                        'order' => 'ASC'
                      ]);
                      foreach ($myposts as $post) : ?>
                      <li class="left__item">
                  		<div class="left__image" style="background-image: url(<?php echo get_field('image')['url']; ?>)">
                  			<a href="<?php the_permalink(); ?>"></a>
                  		</div>
                          <div class="left__employee">
                          	<a href="<?php the_permalink(); ?>">
                          		<?php the_title(); ?>
                          	</a>
                              <span><?php the_field('person_title'); ?></span>
                              <span><?php the_field('sub_title'); ?></span>
                          </div>
                      </li>
                    <?php 
	                    endforeach;
						wp_reset_postdata();
                	?>                
                </ul>
            </div>
            <div class="container-right">
                <div class="container-right__img" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>
                   <?php the_content(); ?>
                <div class="btn-wrapper">
                    <?php 
                        $currentCategory = get_the_category();
                        $subID = $currentCategory[0]->term_id;
                        if ($subID == 8) $subID = $currentCategory[1]->term_id;
                        $prev=thePrev(get_the_ID(), $subID);
                        $next=theNext(get_the_ID(), $subID);
                        $nr = 0;
                    	if($next) { ?>
                    		<a href="<?php echo $next; ?>" class="nav-link">
                          <?php the_field('next_button_text', option); ?>
                        </a>
                    		<?php if($prev) { ?><span class="separator">|</span> <?php $nr = 1; } 
						}
                    	if($prev) { 
                    		if ($next && $nr == 0) { ?> <span class="separator">|</span> <?php } ?>
                    		<a href="<?php echo $prev ?>" class="nav-link">
                          <?php the_field('previous_button_text', option); ?> 
                        </a>  	
                    <?php } ?>
                </div>

            </div>

        </div>

    </div>

</main>
<?php
	} // end while
} // end if
get_footer();