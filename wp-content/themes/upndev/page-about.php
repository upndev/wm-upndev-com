<?php
/**
 * Template name: About
 */

get_header();
if (have_posts()) {
	while (have_posts()) {
		the_post();
?>
  <main class="uber_uns">
        <div class="wrapper">
            <div class="banner"></div>
            <div class="banner--mobile"></div>
            <div class="row"></div>
           	<section class="top">
                <div class="uber_uns__container">
                        <h1 class="header-main u-marginBottom-bigger">
                            <?php the_title(); ?>
                        </h1>
                        <div class="uber_uns__text">
                        	<?php the_content(); ?>
                        </div>
                        <div class="uber_uns__wrapper u_mt--1">
	                        <p class="uber_uns__partner u-marginBottom-small"><?php the_field("above_button"); ?></p>
	                        <a href="<?php the_permalink(137); ?>" class="btn "><?php the_field('button_text'); ?><span>&rarr;</span></a>
	                    </div>
                </div>
            </section>
           
        <?php 
            $args = array(
            	'child_of'	=>	2,
				'orderby'	=>	'ID',
				'order'		=>	'ASC'
				); 
			$categories = get_categories($args);
			foreach($categories as $category) { ?>
				
	             <section class="uber_uns__section">
	                <h2 class="header-second u-marginBottom-small">
	                    <?php echo $category->name ?>
	                </h2>   
		            <div class="uber_uns__container">   
		             	<?php
	                    $myposts = get_posts([
	                      'posts_per_page' => -1,
	                      'category' => $category->term_id,
	                      'orderby' => 'date',
	                      'order' => 'ASC'
	                    ]);
						foreach ($myposts as $post) :
						$personal = get_field('Personal_page'); ?>     
	                      <div class="uber_uns__wrapper u-marginBottom-big">
							  <div style="position: relative; background-size: cover; background-image: url('<?php echo get_field('image')['url'];?>')" class="uber_uns__img">
								<?php if (!$personal) { ?>
									<a href="<?php the_permalink(); ?>" style="position: absolute; left: 0; top: 0; width: 100%; height: 100%"></a>
								 <?php } ?>
	                          </div>
	                          <div class="uber_uns__description">
	                              <div class="slide-title">
								 	<?php if (!$personal) { ?>
	                              		<a href="<?php the_permalink(); ?>">
	                              			<?php the_title(); ?>
										</a>
									<?php } else { ?>
										<span><?php the_title(); ?></span>
									<?php } ?>
	                                  	<span class="slide-title--sub"><?php the_field('person_title'); ?></span>
	                                 	 <span class="slide-title--sub"><?php the_field('sub_title'); ?></span>
	                              </div>
	                              <p class="slide-desc">
	                                  <?php the_field('description'); ?>
		                              <?php if ($category->slug != 'buropersonal') : ?>
                                          <a class="text-link" href="<?php the_permalink(); ?>"><?php the_field('more_link_text',32); ?></a>
		                              <?php endif;?>
	                              </p>
	                          </div>
	                      </div>
	              		<?php endforeach; ?>
		        	</div>
	        	</section>
        	<?php } ?>

        </div>

    </main>

<?php
	} // end while
} // end if
get_footer(); ?>
