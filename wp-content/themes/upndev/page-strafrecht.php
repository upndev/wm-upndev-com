<?php
/**
 * Template name: Strafrecht
 */

get_header();


if (have_posts()) {
	while (have_posts()) {
		the_post();
?>
<main class="starfrecht">
    <div class="wrapper">
        <h1 class="header-main u-marginBottom-big u-marginLeft-h1">
            <?php the_title(); ?>
        </h1>

        <div class="container">
            <div class="container-left">
                <h3 class="header-block"><?php the_field('people_title'); ?></h3>
                <ul class="left__list">
                  <?php
                    $myposts = get_posts([
                      'posts_per_page' => 50,
                      'category' => 6,
                      'orderby' => 'date',
                      'order' => 'ASC'
                    ]);
                    foreach ($myposts as $post): ?>
                    <li class="left__item">
                    	<div class="left__image" style="background-image: url(<?php echo get_field('image')['url']; ?>)">
                      			<a href="<?php the_permalink(); ?>"></a>
                      	</div>
                        <div class="left__employee">
                          	<a href="<?php the_permalink(); ?>">
                          		<?php the_title(); ?>
                          	</a>
                            <span><?php the_field('person_title'); ?></span>
                            <span><?php the_field('sub_title'); ?></span>
                        </div>
                    </li>
                  <?php 
                  endforeach;
                  wp_reset_postdata();
                  ?>
                </ul>

                <h3 class="header-block"><?php the_field('reference_title'); ?></h3>
                <ul class="credentials__list">
                    <?php
	                    $myposts = get_posts([
                            'posts_per_page' => 3,
                            'category' => 10,
                            'orderby' => 'date',
                            'order' => 'DESC'
                          ]);
                        foreach ($myposts as $post) : ?>
                            <li class="credentials__items"><a class="nav-link" href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></li>
                    <?php
                        endforeach; 
                        wp_reset_postdata();
                    ?>
                </ul>
            </div>
            <div class="container-right">
                <div class="starfrecht__intro">
                    <?php
                        the_content();
                    ?>
                </div>
                <h3 class="header-block"><?php the_field('list_title'); ?></h3>
                <ul class="starfrecht__list">

                  <?php
                      //$post = get_post( 12 );
                      //setup_postdata( $post );
                      if( have_rows('list') ):
                        while ( have_rows('list') ) : the_row(); ?>
                          <li class="starfrecht__item">
                              <h2 class="starfrecht__heading"><?php the_sub_field('title'); ?></h2><span class="starfrecht__arrow"></span>
                              <div class="starfrecht__text-block js-toggleList">
                                  <p class="starfrecht__text">
                                      <?php the_sub_field('answer'); ?>
                                  </p>
                              </div>
                          </li>
                        <?php
                      endwhile;
                      endif;
                  ?>
                </ul>
            </div>
        </div>
    </div>
</main>

<?php 

	} // end while
} // end if

get_footer(); ?>


