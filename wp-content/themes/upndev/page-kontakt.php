<?php
/**
 * Template name: Kontakt
 */

get_header();
if (have_posts()) {
	while (have_posts()) {
		the_post();
?>
<main class="contacts">
    <div class="wrapper">
        <h1 class="header-main u-marginBottom-big u-marginLeft-h1">
            <?php the_title(); ?>
        </h1>

        <div class="container">
            <div class="contacts-left">
                <?php the_field('contact_text'); ?>
                <div class="contacts__email">
                    <i id="contacts-icon"></i>
                    <?php the_field('main_email'); ?>
                </div>
                <?php the_field('personal_emails'); ?>
            </div>
            <div class="contacts-right">
                <div class="contacts__map" id="map"></div>
            </div>

        </div>



    </div>

</main>
<script>
	var lat = <?php echo get_field('location')['lat']; ?>;
	var lng = <?php echo get_field('location')['lng']; ?>;
</script>
<?php 
	} // end while
} // end if
get_footer(); ?>