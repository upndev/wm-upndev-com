<?php
/**
 * Template name: Datenschutz
 */

get_header();
if (have_posts()) {
	while (have_posts()) {
		the_post();
?>
<main class="datenschutzerklarung">
    <div class="wrapper">
        <h1 class="header-main u-marginBottom-big u-marginLeft-h1">
            <?php the_title(); ?>
        </h1>

        <div class="container">
            <div class="container-left">
                <?php the_field('contact_field', 'option'); ?>
            </div>
            <div class="container-right">
                	<?php the_content(); ?>
            </div>

        </div>

    </div>

</main>

    

<?php 
	} // end while
} // end if
get_footer(); ?>