<?php
/**
 * Template name: Homepage
 */
get_header();
?>
    <main class="main u-marginBottom-huge">
        <div class="wrapper">
            <h1 class="header-main u-marginBottom-big">
                <?php the_field('homepage_header'); ?>
            </h1>
            <div class="btn-wrapper">
                <a href="<?php the_permalink(12); ?>" class="btn"><?php echo get_the_title(12); ?><span>&rarr;</span></a>
                <a href="<?php the_permalink(14); ?>" class="btn"><?php echo get_the_title(14); ?><span>&rarr;</span></a>
            </div>
            <div class="row">
                <div class="paragraph">
                    <p class="paragraph__heading"><?php the_field('about_header') ?></p>
                    <p class="paragraph__text"><?php the_field('about_description') ?>
                        <a class="text-link" href="<?php the_permalink(32); ?>"><?php echo $GLOBALS['text_more'] ?></a></p>

                </div>
            </div>

            <div class="swiper-container row swiper-1">
                <div class="swiper-pagination swiper-pagination--left"></div>
                <div class="swiper-wrapper">

                	<?php
						/*$myposts = get_posts([
							'posts_per_page' => -1,
							'category' => 2,
				            'orderby' => 'date',
				            'order' => 'ASC'
						]);*/
						$myposts=[];
						$args = array(
							'child_of'	=>	2,
							'orderby'	=>	'ID',
							'order'		=>	'ASC');
						$categories = get_categories( $args );
						foreach($categories as $category) { 							
							$items = get_posts([
								'posts_per_page' => -1,
								'category' => $category->term_id,
					            'orderby' => 'date',
					            'order' => 'ASC'
							]);
							$myposts = array_merge($myposts,$items);
						}
						foreach ($myposts as $post):
								$personal = get_field('Personal_page');
								$homepage = get_field("dont_show_on_homepage");
								if(!$homepage) :
							?>

		                    <div class="swiper-slide">
								<div class="swiper-img"  style="position: relative; background-size: cover; background-image: url('<?php echo get_field('image')['url'];?>')">
									<?php if (!$personal) { ?>
										<a href="<?php the_permalink(); ?>" style="position: absolute; left: 0; top: 0; width: 100%; height: 100%"></a>
									<?php } ?>
		                        </div>
		                        <div class="slide-title">
									<?php if (!$personal) { ?>
		                        	<a href="<?php the_permalink(); ?>">
		                        		<?php the_title(); ?> 
									</a>
									<?php } else { ?>
										<span><?php the_title(); ?></span>
									<?php } ?>
		                        	<span class="slide-title--sub"><?php if(get_field('sub_title')) the_field('sub_title'); else the_field('person_title'); ?></span>
		                        </div>
		                    </div>

                    <?php 
                    	endif;
                	    endforeach;
						wp_reset_postdata();
					 ?>
                </div>
            </div>

            <?php
        		$myposts = get_posts([
					'posts_per_page' => 5,
					'category' => 3,
				]);
				?>

            <div class="row">
                <div class="swiper-testimonial swiper-2 <?php if ($myposts[1]) : ?>js-swiper-2<?php endif; ?>">
                    <h2 class="testimonial__heading"><?php the_field('reviews_title'); ?></h2>
                    <div class="swiper-wrapper">

                    	<?php
                    		$myposts = get_posts([
								'posts_per_page' => 5,
								'category' => 3,
							]);
							foreach ($myposts as $post) :
								setup_postdata($post);	?>
									<div class="swiper-slide">
			                            <div class="testimonial__text">
			                                <?php the_content(); ?>
			                            </div>
			                            <p class="testimonial__author"><?php the_title(); ?></p>
			                        </div>
		                    <?php endforeach; ?>

                    </div>
                    <div class="swiper-pagination swiper-pagination--right"></div>
                </div>

            </div>


        </div>

    </main>



<?php get_footer(); ?>
