var swiper, swiper2,



    setLayouts = debounce(function() {
        // do stuff
    }, 250);

function swippers(){
    FastClick.attach(document.body);
    // gallery swiper
    var group = $(window).width() > 768 ? 2 : 1;
    swiper = new Swiper('.swiper-1', {
        slidesPerView: group,
        slidesPerGroup: group,
        speed: 600,
        autoplay: {
            delay: 3000,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
        },
    });

    swiper2 = new Swiper('.js-swiper-2', {
        slidesPerView: 1,
        speed: 600,
        allowTouchMove: false,
        autoplay: {
            delay: 5000,
        },
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
        },
    });


};

$(window).resize(setLayouts);

// debounce
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    }
}


$('.starfrecht__item').each(function () {
    var text = $(this).find('.starfrecht__text').text().replace(/\s+/g, '');
    if(text.length === 0) {
        $(this).children(".starfrecht__arrow").hide();
    } else {
        $(this).click(function () {
            $(this).children(".js-toggleList").slideToggle(200);
            $(this).children(".starfrecht__arrow").toggleClass('rotated');
            $(this).children(".starfrecht__heading").toggleClass('changeFz');
        });
    }
});



const closeSVG = '<svg id="bars" class="js-menu" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 352.2 352.2" enable-background="new 0 0 352.2 352.2" xml:space="preserve"><path d="M0,317.5l34.7,34.7L352.2,34.7L317.5,0L0,317.5z"/><path d="M34.7,0L0,34.7l315,315l34.7-34.7L34.7,0z"/></svg>';
const barsSVG = '<svg id="bars" class="js-menu"version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 12" enable-background="new 0 0 18 12" xml:space="preserve"><path d="M0,12h18v-2H0V12z M0,7h18V5H0V7z M0,0v2h18V0H0z"/></svg>';



$('.js-btnToggle').click(function (e) {
    e.preventDefault();
    $('.maiNav').toggleClass('is-active');
    if ($('.maiNav').hasClass('is-active')) {
        $('.js-btnToggle').html(closeSVG);
    } else {
        $('.js-btnToggle').html(barsSVG);
    }

})



$('html').click(function(e) {
    if (($(e.target).closest('.maiNav').length === 0) && (!$(e.target).hasClass('js-btnToggle'))) {
        $('.maiNav').removeClass('is-active');
        $('.js-btnToggle').html(barsSVG);

    }
});


//-----------MEDIA QUERY FOR JS---------------

// media query event handler
if (matchMedia) {
    const mq = window.matchMedia("(min-width: 768px)");

    mq.addListener(WidthChange);
    WidthChange(mq);
}



// media query change
function WidthChange(mq) {
    if (mq.matches) {
        // console.log('desktop');
        if (swiper) {swiper.destroy( true, true );}
        if (swiper2) {swiper2.destroy( true, true );}


        swippers();






    } else {
        // console.log('mobile');
        if (swiper) {swiper.destroy( true, true );}
        if (swiper2) {swiper2.destroy( true, true );}
        swippers();


    }

}

//Disclaimer close
$('#closeDisclaimer').click(function (e) {
    e.preventDefault();
    $('.disclaimer').fadeOut();
    $('body').removeClass('hasDisclaimer');
    Cookies.set('disclaimerHide', 'true', { expires: 365 });
})

