<?php
/**
 * Up'n'dev functions and definitions
 *
 */

if (!function_exists('upndev_setup')):
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function upndev_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

	}
endif;
add_action( 'after_setup_theme', 'upndev_setup' );



// Enqueue scripts and styles.
function upndev_scripts() {
	// css
	wp_enqueue_style('style', get_template_directory_uri() . '/css/frontend.min.css');
	wp_enqueue_style('style-add', get_template_directory_uri() . '/css/add.css');
	// plugin js
	wp_enqueue_script( 'upndev-plugins', get_template_directory_uri() . '/js/plugins.min.js', [], '1', true );
	// js
	wp_enqueue_script( 'upndev', get_template_directory_uri() . '/js/frontend.js', [], '1', true );
	wp_enqueue_script( 'upndev-add', get_template_directory_uri() . '/js/add.js', [], '1', true );
	wp_enqueue_script( 'upndev-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDXk182FI3ZGc2gcZZm04TALvgOSlE9VzA&callback=initMap', ['upndev-add'], '1', true );

}
add_action( 'wp_enqueue_scripts', 'upndev_scripts' );



// Remove emoji icons
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');



// Remove wp-embed script
function my_deregister_scripts(){
  	wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );



// Hide admin bar
add_filter('show_admin_bar', '__return_false');



// Disable admin menu items
function remove_menus(){
  remove_menu_page('edit-comments.php'); // Comments
  // remove_menu_page('themes.php'); // Appearance
}
add_action( 'admin_menu', 'remove_menus' );



// Support single category tamplates
function get_custom_cat_template($single_template) {
   	global $post;
   	$files = scandir(__DIR__);
   	foreach ($files as $file) {
		if (strpos($file, 'single-') !== false) {
		    $name = str_replace('single-', '', $file);
		    $name = str_replace('.php', '', $name);
		   	if (in_category($name)) {
				$single_template = dirname( __FILE__ ).'/single-'.$name.'.php';
		   	}
		}
   	}
   	return $single_template;
}
add_filter('single_template','get_custom_cat_template');

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

//remove_filter( 'the_content', 'wpautop' );

/*
Ajax example

function fooName() {}
add_action('wp_ajax_fooName', 'ajax_fooName');
add_action('wp_ajax_nopriv_fooName', 'ajax_fooName');

 */
 
// For ACF google map support

function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyDXk182FI3ZGc2gcZZm04TALvgOSlE9VzA';
	return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
 
 
 function thePrev($id, $num){

	$pagelist=[];
	$args = array('child_of'	=>	$num,
				'orderby'	=>	'ID',
				'order'		=>	'ASC');
	$categories = get_categories( $args );
	foreach($categories as $category) { 							
		$items = get_posts([
			'posts_per_page' => -1,
			'category' => $category->term_id,
	        'orderby' => 'date',
	        'order' => 'ASC'
		]);
		$pagelist = array_merge($pagelist, $items);
	}
	$pages = array();
	foreach ($pagelist as $page) {
		$personal = get_field('Personal_page',$page->ID);
		if (!$personal) {
		   	$pages[] += $page->ID;
		}
	}

	$current = array_search($id, $pages);
	$prevID = $pages[$current-1];
	
	if (!empty($prevID)) {
		return get_permalink($prevID);
	}
}
function theNext($id, $num){
		$pagelist=[];
		$args = array('child_of'	=>	$num,
				'orderby'	=>	'ID',
				'order'		=>	'ASC');
		$categories = get_categories( $args );
		foreach($categories as $category) { 							
			$items = get_posts([
				'posts_per_page' => -1,
				'category' => $category->term_id,
		        'orderby' => 'date',
		        'order' => 'ASC'
			]);
			$pagelist = array_merge($pagelist, $items);
		}
		$pages = array();
		foreach ($pagelist as $page) {
			$personal = get_field('Personal_page',$page->ID);
			if (!$personal) {
			   $pages[] += $page->ID;
			}
		}
	
		$current = array_search($id, $pages);
		$nextID = $pages[$current+1];
		
		if (!empty($nextID)) {
			return get_permalink($nextID);
	}
}
