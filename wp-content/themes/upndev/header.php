<?php
/**
 * The header for our theme
 *
 */

define('LNG',qtrans_getLanguage());
$GLOBALS['text_more'] = LNG == 'de' ? 'Mehr' : 'More';

 
?><!doctype html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="msapplication-TileColor" content="#FFFFFF">
	<meta property="og:url" content="<?php the_permalink() ?>">
	<link href="/favicon.ico" type="image/x-icon" rel="icon">
	<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon">
	<?php /*
	<meta name="description" content="<?php the_field('site_description'); ?>">
	<meta itemprop="name" content="<?php the_field('site_title'); ?>">
	<meta itemprop="description" content="<?php the_field('site_description'); ?>">
	<meta property="og:title" content="<?php the_field('site_title'); ?>">
	<meta property="og:description" content="<?php the_field('site_description'); ?>">
	<meta itemprop="image" content="<?php echo get_field('site_image')['url']; ?>">
	<meta property="og:image" content="<?php echo get_field('site_image')['url']; ?>">
	<meta name="twitter:image:src" content="<?php echo get_field('site_image')['url']; ?>">
	 */
	?>
	<?php wp_head(); ?>
</head>
<body <?php if (!isset($_COOKIE["disclaimerHide"])) {
                 body_class('hasDisclaimer');
            } else {
                body_class();
            }
        ?>>
	<?php if (get_the_ID() == 32): ?>
    	<div style="background-image: url(<?php echo get_field('desktop_photo')['url']; ?>)" class="banner"></div>
	    <div style="background-image: url(<?php echo get_field('mobile_photo')['url']; ?>)" class="banner--mobile"></div>
	<?php endif; ?>
	
	<div id="bg">
        <i></i>
        <i></i>
        <i></i>
    </div>

    
	<div id="page">
		
	<div class="wrapper">
		<header class="header">
            <div class="wrapper">
                <a href="<?php echo site_url(); ?>" class="logoContainer">
                    <svg id="logo" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1479 176.7" enable-background="new 0 0 1479 176.7" xml:space="preserve"><g><path d="M355.5,95.6l12.4-40.2h16.3l12.3,40.2l13.9-40.2h16.9l-24.4,69.9h-11.6L376,76.7l-15.2,48.6h-11.6l-24.4-69.9h16.9L355.5,95.6z"/><path  d="M496,55.4v13.9h-34.8v14.4h31.3V97h-31.3v14.5h35.9v13.8h-51.5V55.4H496z"/><path d="M521,55.4h15.6v69.9H521V55.4z"/><path  d="M626.4,81.3l-18.9,38.2h-9.3l-18.8-38.2v44h-15.6V55.4h21.1l18,38.4L621,55.4h21v69.9h-15.6V81.3z"/><path d="M719,125.3l-6.5-15.1h-29.3l-6.5,15.1h-16.6l30.2-69.9h15.1l30.2,69.9H719z M697.9,76.2l-8.8,20.3h17.5L697.9,76.2z"/><path  d="M802.6,55.4h15.6v69.9h-15.6l-33.3-43.8v43.8h-15.6V55.4h14.6l34.3,45V55.4z"/><path  d="M894.3,55.4h15.6v69.9h-15.6L861,81.5v43.8h-15.6V55.4H860l34.3,45V55.4z"/><path  d="M1014.3,119.3c-5.6,4.5-12.1,6.8-19.4,6.8c-7.4,0-13.4-1.9-18.2-5.6c-4.7-3.8-7.1-8.9-7.1-15.5c0-7.3,4.9-13.8,14.7-19.5c-2.2-2.7-3.8-5-4.8-7c-1-2-1.5-4.4-1.5-7.1c0-5.7,2-10,6.1-13s9-4.5,14.5-4.5c5.6,0,10.4,1.4,14.4,4.3c4,2.9,6,6.9,6,12.2c0,6.5-4.8,12.6-14.3,18.2c3.7,4.7,6.5,8,8.4,10.1c1.7-3.1,2.9-6.3,3.8-9.7l11.7,8.4c-1.5,4.2-3.4,8.1-5.6,11.7l9.9,9.8l-9.9,9.4L1014.3,119.3z M987.4,111.3c1.9,1.5,4.4,2.3,7.5,2.3c3.1,0,6.4-1.5,9.9-4.4c-4.9-5.1-9.1-9.8-12.6-14c-5.1,3.2-7.7,6.6-7.7,10.3C984.5,107.8,985.5,109.8,987.4,111.3z M998.7,65.6c-1.5,0-2.8,0.5-3.9,1.6c-1.2,1-1.8,2.4-1.8,4.2c0,1.8,1.3,4.3,4,7.7c5-2.7,7.5-5.2,7.5-7.5c0-1.9-0.6-3.4-1.8-4.4C1001.5,66.1,1000.2,65.6,998.7,65.6z"/><path  d="M1152.5,81.3l-18.9,38.2h-9.3l-18.8-38.2v44h-15.6V55.4h21.1l18,38.4l18.1-38.4h21v69.9h-15.6V81.3z"/><path  d="M1245.7,55.4v13.9h-34.8v14.4h31.3V97h-31.3v14.5h35.9v13.8h-51.5V55.4H1245.7z"/><path  d="M1300.8,125.3h-15.6V97.7L1261,55.4h16.9l15.1,26l15.1-26h16.9l-24.2,42.3V125.3z"/><path  d="M1392.9,55.4v13.9h-34.8v14.4h31.3V97h-31.3v14.5h35.9v13.8h-51.5V55.4H1392.9z"/><path  d="M1474.6,78.6c0,11.2-4.4,18.4-13.3,21.7l17.7,25h-19.2l-15.5-22.3h-10.8v22.3h-15.6V55.4h26.5c10.9,0,18.6,1.8,23.3,5.5C1472.3,64.6,1474.6,70.5,1474.6,78.6z M1455.8,86.9c1.9-1.7,2.9-4.5,2.9-8.3c0-3.8-1-6.3-3-7.8c-2-1.4-5.5-2.1-10.5-2.1h-11.7v20.7h11.4C1450.2,89.5,1453.9,88.6,1455.8,86.9z"/></g><g><polygon  points="217.1,176.7 0,176.7 0,0 15,0 15,161.7 202.1,161.7 202.1,55.4 176.7,55.4 176.7,136.2 121.2,136.2 121.2,55.4 95.8,55.4 95.8,136.2 40.4,136.2 40.4,0 257.5,0 257.5,176.7 242.5,176.7 242.5,15 55.4,15 55.4,121.2 80.8,121.2 80.8,40.4 136.2,40.4 136.2,121.2 161.7,121.2 161.7,40.4 217.1,40.4 "/></g></svg>
                </a>
                <div class="action-buttons">
                    <a href="tel:<?php the_field('phone', 'option'); ?>" id="phone" class="icon-border">
                        <svg id="phone-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="2645.1 38.1 580.5 580.5"><g><g><path  d="M3086.5,422.2c-19-18.7-42.6-18.7-61.5,0c-14.4,14.3-28.8,28.5-42.9,43c-3.9,4-7.1,4.8-11.8,2.2c-9.3-5.1-19.2-9.2-28.1-14.7c-41.7-26.2-76.6-59.9-107.5-97.8c-15.3-18.8-29-39-38.5-61.7c-1.9-4.6-1.6-7.6,2.2-11.4c14.4-13.9,28.4-28.1,42.5-42.4c19.7-19.8,19.7-43-0.1-62.9c-11.2-11.4-22.5-22.5-33.7-33.8c-11.6-11.6-23.1-23.3-34.8-34.8c-19-18.5-42.6-18.5-61.5,0.1c-14.5,14.3-28.4,28.9-43.1,42.9c-13.7,12.9-20.5,28.8-22,47.2c-2.3,30.1,5.1,58.5,15.5,86.1c21.3,57.3,53.6,108.1,92.9,154.7c53,63.1,116.3,112.9,190.4,148.9c33.3,16.2,67.9,28.6,105.5,30.7c25.8,1.5,48.3-5.1,66.3-25.2c12.3-13.8,26.2-26.3,39.3-39.5c19.3-19.6,19.4-43.2,0.2-62.6C3132.6,468.1,3109.5,445.1,3086.5,422.2z"/><path  d="M3063.4,325.9l44.6-7.6c-7-41-26.3-78-55.7-107.5c-31-31-70.3-50.6-113.6-56.7l-6.3,44.8c33.5,4.7,63.9,19.8,87.9,43.9C3043.1,265.5,3058,294.2,3063.4,325.9z"/><path  d="M3133.1,132.1c-51.5-51.5-116.6-84-188.5-94l-6.3,44.8c62.1,8.7,118.4,36.8,162.8,81.2c42.2,42.2,69.8,95.4,79.9,154l44.6-7.6C3213.9,242.6,3181.9,181,3133.1,132.1z"/></g></g></svg>
                    </a>
                    <a href="mailto:<?php the_field('mail', 'option'); ?>" id="mail" class="icon-border">
                        <svg id="mail-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="1527.8 105.8 550.8 432.8" enable-background="new 1527.8 105.8 550.8 432.8" xml:space="preserve"><g><g><path d="M2029.4,538.5c12.4,0,23.1-4.1,32.2-12.2l-156-156.1c-3.7,2.7-7.4,5.3-10.8,7.8c-11.7,8.6-21.2,15.3-28.4,20.1c-7.3,4.8-17,9.7-29,14.8c-12.1,5-23.4,7.5-33.8,7.5h-0.3h-0.3c-10.5,0-21.7-2.5-33.8-7.5c-12.1-5-21.8-9.9-29-14.8c-7.3-4.8-16.8-11.5-28.4-20.1c-3.3-2.4-6.9-5-10.8-7.8l-156.1,156.1c9.1,8.1,19.9,12.2,32.2,12.2H2029.4z"/><path  d="M1558.8,272c-11.7-7.8-22-16.7-31-26.7v237.4l137.5-137.5C1637.8,326,1602.3,301.6,1558.8,272z"/><path  d="M2047.8,272c-41.9,28.3-77.4,52.7-106.8,73.2l137.5,137.5V245.3C2069.7,255.1,2059.5,264.1,2047.8,272z"/><path  d="M2029.4,105.8h-452.4c-15.8,0-27.9,5.3-36.4,16c-8.5,10.7-12.8,24-12.8,40c0,12.9,5.6,26.9,16.9,42c11.3,15.1,23.3,26.9,36,35.5c7,4.9,28,19.5,63,43.8c18.9,13.1,35.4,24.5,49.5,34.4c12,8.4,22.4,15.7,31,21.7c1,0.7,2.5,1.8,4.6,3.3c2.2,1.6,5,3.6,8.5,6.1c6.7,4.8,12.2,8.7,16.6,11.7c4.4,3,9.7,6.3,16,10c6.2,3.7,12.1,6.5,17.7,8.3c5.5,1.8,10.7,2.8,15.4,2.8h0.3h0.3c4.7,0,9.8-0.9,15.4-2.8c5.5-1.8,11.4-4.6,17.7-8.3c6.2-3.7,11.6-7,16-10c4.4-3,9.9-6.9,16.6-11.7c3.5-2.5,6.3-4.5,8.5-6.1c2.1-1.5,3.6-2.6,4.6-3.3c6.7-4.7,17.1-11.9,31.1-21.6c25.5-17.7,63-43.8,112.7-78.3c15-10.4,27.5-23.1,37.5-37.8c10-14.7,15.1-30.2,15.1-46.4c0-13.5-4.9-25.1-14.6-34.7C2054.2,110.6,2042.7,105.8,2029.4,105.8z"/></g></g></svg>
                    </a>
                    <a class="desktopContacts" href="tel:<?php the_field('phone', 'option'); ?>"><?php the_field('phone', 'option'); ?></a>
                    <a class="desktopContacts" href="mailto:<?php the_field('mail', 'option'); ?>"><?php the_field('mail', 'option'); ?></a>
                    <a href="#" class="js-btnToggle">
                        <svg id="bars" class="js-menu"version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 12" enable-background="new 0 0 18 12" xml:space="preserve"><path d="M0,12h18v-2H0V12z M0,7h18V5H0V7z M0,0v2h18V0H0z"/></svg>
                    </a>
                </div>
                <nav class="maiNav">
                    <ul class="maiNav__menu">

                        <?php
                            $items = wp_get_nav_menu_items('Menu');
                            foreach ($items as $item) {
                                if (get_the_ID() == $item->object_id)
                                    $active = ' maiNav__link--active';
                                else
                                    $active='';
                                echo '<li class="maiNav__menuItem"><a class="maiNav__link'.$active.'" href="'.$item->url.'">'. $item->title.'</a></li>';
                            }
                        ?>


                    </ul>
                        <div class="mainNav_lang">
                            <a href="<?php echo site_url(); ?>/de/" class="maiNav__link<?php if(LNG == 'de') echo " maiNav__link--active"?> ">De</a>
                            <a href="<?php echo site_url(); ?>/en/" class="maiNav__link<?php if(LNG == 'en') echo " maiNav__link--active"?> u-marginLeft-vSmall">En</a>
                        </div>
                </nav>
            </div>
</header>
		<?php if (!isset($_COOKIE["disclaimerHide"])) :?>

        <div class="disclaimer">
            <p>
                Diese Seite verwendet Cookies und Analysetools, beginnend mit Ihrer Zustimmung durch Klick auf <a
                        href="#" id="closeDisclaimer">“Weiter”</a>. Weitere Infos finden Sie in unserer <a href="<?php the_permalink(34) ?>">Datenschutzerklärung</a>.
            </p>
        </div>

        <?php endif; ?>