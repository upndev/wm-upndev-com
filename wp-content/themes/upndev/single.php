<?php
/**
 * 
 */

$lng = qtrans_getLanguage();

get_header();
if (have_posts()) {
	while (have_posts()) {
		the_post();
		
		
	$personal = get_field('Personal_page');
	if ($personal) {
		echo '<script>window.location.href = "/";</script>';
		die;
	}
?>

<main class="person">
    <div class="wrapper">
		<div class="mobileImg" style="background-image: url(<?php echo get_field('image')['url'];?>)"></div>
        <h1 class="header-main u-marginLeft-h1 u-marginBottom-bigger">
            <?php the_title(); ?>
            <span class="header-main--sub"><?php if(get_field('sub_title')) the_field('sub_title'); else the_field('person_title'); ?></span>
        </h1>

        <div class="container">
            <div class="container-left">
            	<?php if(get_field('phone')) : ?>
	                <a href="tel:<?php the_field('phone'); ?>" class="person__contacts">
	                    <i class="icon">
	                        <svg class="phone-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="2645.1 38.1 580.5 580.5"><g><g><path  d="M3086.5,422.2c-19-18.7-42.6-18.7-61.5,0c-14.4,14.3-28.8,28.5-42.9,43c-3.9,4-7.1,4.8-11.8,2.2c-9.3-5.1-19.2-9.2-28.1-14.7c-41.7-26.2-76.6-59.9-107.5-97.8c-15.3-18.8-29-39-38.5-61.7c-1.9-4.6-1.6-7.6,2.2-11.4c14.4-13.9,28.4-28.1,42.5-42.4c19.7-19.8,19.7-43-0.1-62.9c-11.2-11.4-22.5-22.5-33.7-33.8c-11.6-11.6-23.1-23.3-34.8-34.8c-19-18.5-42.6-18.5-61.5,0.1c-14.5,14.3-28.4,28.9-43.1,42.9c-13.7,12.9-20.5,28.8-22,47.2c-2.3,30.1,5.1,58.5,15.5,86.1c21.3,57.3,53.6,108.1,92.9,154.7c53,63.1,116.3,112.9,190.4,148.9c33.3,16.2,67.9,28.6,105.5,30.7c25.8,1.5,48.3-5.1,66.3-25.2c12.3-13.8,26.2-26.3,39.3-39.5c19.3-19.6,19.4-43.2,0.2-62.6C3132.6,468.1,3109.5,445.1,3086.5,422.2z"/><path  d="M3063.4,325.9l44.6-7.6c-7-41-26.3-78-55.7-107.5c-31-31-70.3-50.6-113.6-56.7l-6.3,44.8c33.5,4.7,63.9,19.8,87.9,43.9C3043.1,265.5,3058,294.2,3063.4,325.9z"/><path  d="M3133.1,132.1c-51.5-51.5-116.6-84-188.5-94l-6.3,44.8c62.1,8.7,118.4,36.8,162.8,81.2c42.2,42.2,69.8,95.4,79.9,154l44.6-7.6C3213.9,242.6,3181.9,181,3133.1,132.1z"/></g></g></svg>
	                    </i>
	                    <?php the_field('phone'); ?>
	                </a>
               <?php 
               	endif;
               	if(get_field('email')) : 	
               ?>
                <a href="mailto:<?php the_field('email'); ?>" class="person__contacts">
                    <i class="icon">
                        <svg class="mail-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="1527.8 105.8 550.8 432.8" enable-background="new 1527.8 105.8 550.8 432.8" xml:space="preserve"><g><g><path d="M2029.4,538.5c12.4,0,23.1-4.1,32.2-12.2l-156-156.1c-3.7,2.7-7.4,5.3-10.8,7.8c-11.7,8.6-21.2,15.3-28.4,20.1c-7.3,4.8-17,9.7-29,14.8c-12.1,5-23.4,7.5-33.8,7.5h-0.3h-0.3c-10.5,0-21.7-2.5-33.8-7.5c-12.1-5-21.8-9.9-29-14.8c-7.3-4.8-16.8-11.5-28.4-20.1c-3.3-2.4-6.9-5-10.8-7.8l-156.1,156.1c9.1,8.1,19.9,12.2,32.2,12.2H2029.4z"/><path  d="M1558.8,272c-11.7-7.8-22-16.7-31-26.7v237.4l137.5-137.5C1637.8,326,1602.3,301.6,1558.8,272z"/><path  d="M2047.8,272c-41.9,28.3-77.4,52.7-106.8,73.2l137.5,137.5V245.3C2069.7,255.1,2059.5,264.1,2047.8,272z"/><path  d="M2029.4,105.8h-452.4c-15.8,0-27.9,5.3-36.4,16c-8.5,10.7-12.8,24-12.8,40c0,12.9,5.6,26.9,16.9,42c11.3,15.1,23.3,26.9,36,35.5c7,4.9,28,19.5,63,43.8c18.9,13.1,35.4,24.5,49.5,34.4c12,8.4,22.4,15.7,31,21.7c1,0.7,2.5,1.8,4.6,3.3c2.2,1.6,5,3.6,8.5,6.1c6.7,4.8,12.2,8.7,16.6,11.7c4.4,3,9.7,6.3,16,10c6.2,3.7,12.1,6.5,17.7,8.3c5.5,1.8,10.7,2.8,15.4,2.8h0.3h0.3c4.7,0,9.8-0.9,15.4-2.8c5.5-1.8,11.4-4.6,17.7-8.3c6.2-3.7,11.6-7,16-10c4.4-3,9.9-6.9,16.6-11.7c3.5-2.5,6.3-4.5,8.5-6.1c2.1-1.5,3.6-2.6,4.6-3.3c6.7-4.7,17.1-11.9,31.1-21.6c25.5-17.7,63-43.8,112.7-78.3c15-10.4,27.5-23.1,37.5-37.8c10-14.7,15.1-30.2,15.1-46.4c0-13.5-4.9-25.1-14.6-34.7C2054.2,110.6,2042.7,105.8,2029.4,105.8z"/></g></g></svg>
                    </i>
                    <?php the_field('email'); ?>
                </a>
               <?php
                endif;
	            if(get_field('vcard_file')) : ?>
                    <a href="<?php  echo get_field('vcard_file')['url']; ?>" class="person__contacts">
                        <i class="icon">
                            <svg class="mail-icon" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1344 1344q0-26-19-45t-45-19-45 19-19 45 19 45 45 19 45-19 19-45zm256 0q0-26-19-45t-45-19-45 19-19 45 19 45 45 19 45-19 19-45zm128-224v320q0 40-28 68t-68 28h-1472q-40 0-68-28t-28-68v-320q0-40 28-68t68-28h465l135 136q58 56 136 56t136-56l136-136h464q40 0 68 28t28 68zm-325-569q17 41-14 70l-448 448q-18 19-45 19t-45-19l-448-448q-31-29-14-70 17-39 59-39h256v-448q0-26 19-45t45-19h256q26 0 45 19t19 45v448h256q42 0 59 39z"/></svg>
                        </i>
			            <?php the_field( 'vcard_text' ); ?>
                    </a>
	            <?php endif; ?>
            </div>
            <div class="container-right">
                <div class="container-right__img" style="background-image: url(<?php echo get_field('image')['url'];?>)"></div>
						<p class="container-right__text">
							<?php the_field('description'); ?>
						</p>
						<div class="container-right__text">
							<?php the_content(); ?>
						</div>
						<?php
							$currentCategory = get_the_category();
							$subID = 2;//$currentCategory[1]->term_id;
							$prev=thePrev(get_the_ID(), $subID);
							$next=theNext(get_the_ID(), $subID);
							$nr = 0;
						?>
                <div class="btn-wrapper">
                    <?php 
                    	if($next) { ?>
                    		<a href="<?php echo $next; ?>" class="nav-link">
                                <?php the_field('next_button_text', option); ?>
                            </a>
                    		<?php if($prev) { ?><span class="separator">|</span> <?php $nr = 1; } 
						}
                    	if($prev) { 
                    		if ($next && $nr == 0) { ?> <span class="separator">|</span> <?php } ?>
                    		<a href="<?php echo $prev ?>" class="nav-link">
                                <?php the_field('previous_button_text', option); ?>                              
                            </a>  	
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</main>
<?php
	} // end while
} // end if
get_footer();
